#include "Arduino.h"
#include "encoder.h"
#include "eeprom.h"
#include "motion.h"
#include "input.h"
#include "indicator.h"
#include <Adafruit_MCP4725.h>

Adafruit_MCP4725 motion;

#define DAC_RESOLUTION    (8) // Set this value to 9, 8, 7, 6 or 5 to adjust the resolution
#define setDirUp digitalWrite(8, HIGH);
#define setDirDown digitalWrite(8, LOW);
#define Row1 pinMode(14, INPUT_PULLUP);
#define Row2 pinMode(15, INPUT_PULLUP);
#define Column1 pinMode(16, INPUT_PULLUP);
#define Column2 pinMode(17, INPUT_PULLUP);
#define Column3 pinMode(9, INPUT_PULLUP);
#define Column4 pinMode(10, INPUT_PULLUP);
#define Column5 pinMode(11, INPUT_PULLUP);
#define FastSlowSwitch pinMode(4, INPUT_PULLUP);
#define LimitSwitch pinMode(7, INPUT_PULLUP);
#define TransistorReverseSwitch pinMode(14, OUTPUT);
#define KeySwitch pinMode(12, INPUT_PULLUP);
#define Indicator pinMode(13, OUTPUT);

int slowZoneTop = 800;
int slowZoneBottom = 600;
int compensationTop = 50;
int compensationBottom = 50;
int breakInProcessBottom = 500;

float kp = 10.0;
float ki = 0.0015;
float kd = 0.5;
unsigned long prevT;
float eprev;
float eintegral;
unsigned long pos;


void moveUp(){
  setDirUp;
}

void moveDown(){
  setDirDown;
}

void moveStop(){
  motion.setVoltage(0, false);
}

void moveFast(){
  motion.setVoltage(4095, false);
}

void moveSlow(){
  motion.setVoltage(2000, false);
}

void initMotionSystem(){
  
  Row1; Row2; Column1; Column2;
  Column3; Column4; Column5;
  FastSlowSwitch; LimitSwitch;
  TransistorReverseSwitch;
  KeySwitch; Indicator;
  
  motion.begin(0x62);
  
  moveStop();
}

void breakInProcess(){
  for (int i = 0; i < 10; i++) {
    for (int i = 0; i < 3; i++) {
      ledBlink(100);
    }
  }
  calibrateEncoder();
  for (int full = 0; full < 15; full++) {
    while (getEncoderPosition() < getTotalStroke()) {
      getEncoderPosition();
      getTotalStroke();
      moveUp();
    }
    while (getEncoderPosition() > breakInProcessBottom) {
      getEncoderPosition();
      moveDown();
    }
    while (getEncoderPosition() < (getTotalStroke() / 2)) {
      getEncoderPosition();
      getTotalStroke();
      moveUp();
    }
    while (getEncoderPosition() > breakInProcessBottom) {
      getEncoderPosition();
      moveDown();
    }
    moveStop();
    ledBreakInProcessDone();
  }
}

void calibrateEncoder(){
  if (getLimitSwitchState() == true) {
    moveUp();
    moveFast();
    delay(3000);
    moveStop();
  }
  while (getLimitSwitchState() != true) {
    moveFast();
    moveDown();
    getLimitSwitchState();
  }
  setEncoder(0);
  ledOn();
  moveStop();
  delay(500);
  ledOff();
}

int positionMove(int buttonNumber){
  while (getInputState(buttonNumber) != true){
    if (getInputState(buttonNumber) == true){ ledOff(); moveStop(); return; }
        
    uint32_t currT = micros();
    float deltaT = ((float) (currT - prevT))/( 1.0e6 );

    float ppm = 2.56;
  
    // error
    int16_t e = getEncoderPosition() - getStoredPosition(buttonNumber);
    // derivative
    float dedt = (e-eprev)/(deltaT);
    // integral
    eintegral = eintegral + e*deltaT;
    // control signal
    float u = kp*e + kd*dedt + ki*eintegral;
    if (u  > 4095) u = 4095;
    if (u < -4095) u = -4095;
    if (u > 0) moveDown();
    if (u < 0) moveUp();
    if (u < 0) u = u * (-1);
    int pwr = map(u, 0, 4095, 1300, 4095);
    int throttle = map(pwr, 0, 4095, 0, 100);
    Serial.print("  pwr: ");
    Serial.println(throttle);
    Serial.print("  u: ");
    Serial.println(u);
    Serial.print(" position. ");
    Serial.print(getEncoderPosition()/ppm);
    Serial.println(" mm ");
    Serial.print(" targetposition: ");
    Serial.print(getStoredPosition(buttonNumber)/ppm);
    Serial.println(" mm ");
    motion.setVoltage(pwr, false);
    eprev = e;
    prevT = currT;
    
  }
  moveStop();
}

void downMove(){
  int downButtonLimit = 5;
  while (getDownButtonState() != true){
    if (getDownButtonState() == true) { moveStop(); return; }

    uint32_t currT = micros();
    float deltaT = ((float) (currT - prevT))/( 1.0e6 );  
  
    // error
    int16_t e = getEncoderPosition(); - downButtonLimit;
    Serial.print("  e: ");
    Serial.println(e);
    // derivative
    float dedt = (e-eprev)/(deltaT);
    // integral
    eintegral = eintegral + e*deltaT;
    // control signal
    float u = kp*e + kd*dedt + ki*eintegral;
    if( u  > 2047 ) u = 2047;
    if( u < -2047 ) u = -2047; 
    int pwr = map(u, 2047, -2047, 4095, 1500);  
    Serial.print("  pwr: ");
    Serial.println(pwr);
    Serial.print(" position. ");
    Serial.println(getEncoderPosition());
    Serial.print(" targetposition: ");
    Serial.println(downButtonLimit);
    if (u > 0) moveDown();
    if (u < 0) moveUp();
    if (getFastSlowState() == true) { motion.setVoltage(pwr, false); }
    else { if (pwr > 3000) motion.setVoltage(pwr/2, false); if (pwr < 2000) motion.setVoltage(pwr, false); }
    eprev = e;
    prevT = currT;
    
  }
  moveStop();
}

void upMove(){
  while (getUpButtonState() != true) {
    if (getUpButtonState() == true) { moveStop(); return; }

    uint32_t currT = micros();
    float deltaT = ((float) (currT - prevT))/( 1.0e6 );  
  
    // error
    int16_t e = getEncoderPosition() - getTotalStroke();
    Serial.print("  e: ");
    Serial.println(e);
    // derivative
    float dedt = (e-eprev)/(deltaT);
    // integral
    eintegral = eintegral + e*deltaT;
    // control signal
    float u = kp*e + kd*dedt + ki*eintegral;
    if( u  > 2047 ) u = 2047;
    if( u < -2047 ) u = -2047; 
    int pwr = map(u, -2047, 2047, 4095, 1500);  
    Serial.print("  pwr: ");
    Serial.println(pwr);
    Serial.print(" position. ");
    Serial.println(getEncoderPosition());
    Serial.print(" targetposition: ");
    Serial.println(getTotalStroke());
    if (u > 0) moveDown();
    if (u < 0) moveUp();
    if (getFastSlowState() == true) { motion.setVoltage(pwr, false); }
    else { if (pwr > 3000) motion.setVoltage(pwr/2, false); if (pwr < 2000) motion.setVoltage(pwr, false); }
    eprev = e;
    prevT = currT;    
    
  }
  moveStop();
}
