#ifndef __INPUT_H
#define __INPUT_H

_Bool getKeyState();
_Bool getLimitSwitchState();
_Bool getFastSlowState();
_Bool getUpButtonState();
_Bool getDownButtonState();
_Bool getInputState(int inputNumber);


#endif
