#include "Arduino.h"
#include "encoder.h"
#include "eeprom.h"
#include "motion.h"
#include "input.h"
#include "indicator.h"

void ledOn(){
  digitalWrite(13, HIGH);
}

void ledOff(){
  digitalWrite(13, LOW);
}

void ledBlink(int blinkPace){
  ledOn();
  delay(blinkPace);
  ledOff();
  delay(blinkPace);
}

void ledRestoreStroke(){
  for (int i = 0; i < 20; i++){
    ledBlink(50);
  }
}

void ledBreakInProcessDone(){
  _Bool getInput = true;
  while (getInput){
    for (int i = 1; i < 10; i++){
      if (getInputState(i) != true) break;
      if (getUpButtonState() != true) break;
      if (getDownButtonState() != true) break;
      ledBlink(1000);
    }
  }
}
