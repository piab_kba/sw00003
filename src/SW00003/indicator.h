#ifndef __INDICATOR_H
#define __INDICATOR_H

void ledOn();
void ledOff();
void ledBlink(int blinkPace);
void ledRestoreStroke();
void ledBreakInProcessDone();

#endif
