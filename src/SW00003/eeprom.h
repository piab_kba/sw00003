#ifndef __EEPROM_H
#define __EEPROM_H

void initEeprom();
int getStoredPosition(int Position);
int getTotalStroke();
void setTotalStroke(_Bool setBit);
void storePosition(int Position);
void totalStrokeCalibrateHandler();
void positionCalibrateHandler(int Position);

#endif
