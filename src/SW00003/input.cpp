#include "Arduino.h"
#include "encoder.h"
#include "eeprom.h"
#include "motion.h"
#include "input.h"
#include "indicator.h"

#define KSwitch digitalRead(12);
#define LSwitch digitalRead(7);
#define FSwitch digitalRead(4);
#define R1 digitalRead(14);
#define R2 digitalRead(15);
#define C1 digitalRead(16);
#define C2 digitalRead(17);
#define C3 digitalRead(9);
#define C4 digitalRead(10);
#define C5 digitalRead(11);

int threshold = 800;

_Bool getKeyState(){
  _Bool returnState;
  returnState = KSwitch;
  return returnState;
}

_Bool getLimitSwitchState(){
  _Bool returnState;
  returnState = LSwitch;
  return returnState;
}

_Bool getFastSlowState(){
  _Bool returnState;
  returnState = FSwitch;
  return returnState;
}

_Bool getUpButtonState(){
  _Bool returnState;
  if (analogRead(A6) > threshold){returnState = true;}else{returnState = false;}
  return returnState;
}

_Bool getDownButtonState(){
  _Bool returnState;
  if (analogRead(A7) > threshold){returnState = true;}else{returnState = false;}
  return returnState;
}

_Bool getInputState(int inputNumber){
  _Bool returnState;
  _Bool readRow;
  _Bool readColumn;
  switch (inputNumber) {
      case 1: readRow = R1; readColumn = C1; break;
      case 2: readRow = R2; readColumn = C1; break;
      case 3: readRow = R1; readColumn = C2; break;
      case 4: readRow = R2; readColumn = C2; break;
      case 5: readRow = R1; readColumn = C3; break;
      case 6: readRow = R2; readColumn = C3; break;
      case 7: readRow = R1; readColumn = C4; break;
      case 8: readRow = R2; readColumn = C4; break;
      case 9: readRow = R1; readColumn = C5; break;
      case 10: readRow = R2; readColumn = C5; break;
  }
  returnState = readRow & readColumn;
  return returnState;
}
