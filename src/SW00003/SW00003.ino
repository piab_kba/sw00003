#include <avr/eeprom.h>
#include "encoder.h"
#include "eeprom.h"
#include "motion.h"
#include "input.h"
#include "indicator.h"

bool powerOut = true;

void setup() {

  initEeprom();

  initEncoder();

  initMotionSystem();

  Serial.begin(9600);

}

void loop() {
  while (powerOut == true) {
    while (getKeyState() != true) {
      ledOff();
      if (getDownButtonState() != true && getInputState(1) != true) breakInProcess();
      while (getInputState(1) != true) { 
        ledOn();
        if (getKeyState() == true){
          setTotalStroke(false);
          ledRestoreStroke();
        }
      }
    }
    ledOn();
    if (getInputState(1) != true && getInputState(2) != true) {
      ledOff();
      calibrateEncoder();
      powerOut = false;
    }
  }
  
  static unsigned int tmp = 0;
  //getEncoderPosition();
  if ((tmp != getEncoderPosition() && (Serial.availableForWrite() > 60))) tmp = getEncoderPosition();
  
  while (getKeyState() != true) {
  if (getUpButtonState() != true && getInputState(1) != true) totalStrokeCalibrateHandler();
    for (int i = 1; i <= 10; i++){
      if (getInputState(i) != true) positionCalibrateHandler(i);
    }
  }
  
  for (int i = 1; i <= 10; i++){
    if (getInputState(i) != true) positionMove(i);
    if (getUpButtonState() != true) Serial.println(getTotalStroke());
    if (getUpButtonState() != true) upMove();
    if (getDownButtonState() != true) downMove();
  }
}
