#include "Arduino.h"
#include "encoder.h"
#include "eeprom.h"
#include "motion.h"
#include "input.h"
#include "indicator.h"

#define EE_STEP_ADDR 4 /* Might want 8 tbh */
#define TOTAL_STROKE_STORED_ADDR_SLOT 10
#define TOTAL_STROKE_ADDR_SLOT 11
#define TOTAL_STROKE 27566

uint32_t written_E[12];

int E[12];

unsigned int totalStroke;
_Bool totalStrokeStored;

void initEeprom(){
  for (int i = 0; i <= 12; i++){
    written_E[i] = eeprom_read_dword((const uint32_t *)(EE_STEP_ADDR * i));
    E[i] = written_E[i];
    if (i == 10){ totalStrokeStored = E[i]; }
  }
  if(totalStrokeStored == true){ totalStroke = E[11]; }
  else{ totalStroke = TOTAL_STROKE; }
}

int getStoredPosition(int Position){
  unsigned int returnValue;
  returnValue = E[Position - 1];
  return returnValue;
}

int getTotalStroke(){
  unsigned int returnValue;
  returnValue = totalStroke;
  return returnValue;
}

void setTotalStroke(_Bool setBit){
  totalStrokeStored = setBit;
  eeprom_write_dword((uint32_t *)(EE_STEP_ADDR * TOTAL_STROKE_STORED_ADDR_SLOT), totalStrokeStored);
  E[TOTAL_STROKE_STORED_ADDR_SLOT] = totalStrokeStored;
  written_E[TOTAL_STROKE_STORED_ADDR_SLOT] = totalStrokeStored;
  if(totalStrokeStored == true){ totalStroke = getEncoderPosition(); }
  else{ totalStroke = TOTAL_STROKE; }
  eeprom_write_dword((uint32_t *)(EE_STEP_ADDR * TOTAL_STROKE_ADDR_SLOT), totalStroke);
  E[TOTAL_STROKE_ADDR_SLOT] = totalStroke;
  written_E[TOTAL_STROKE_ADDR_SLOT] = totalStroke;
}

void storePosition(int Position){
  E[Position - 1] = getEncoderPosition();
  eeprom_write_dword((uint32_t *)(EE_STEP_ADDR * (Position - 1)), E[Position - 1]);
  written_E[Position - 1] = E[Position - 1];
}

void totalStrokeCalibrateHandler(){
  unsigned int i;
  for (i = 0; i < 3; i++) {
    ledBlink(2000);
    if (getUpButtonState() == true) {
      break;
    }
    if (getInputState(1) == true) {
      break;
    }
  }
  if (i > 2) {
    for (int i = 0; i < 10; i++) {
      ledBlink(50);
    }
    setTotalStroke(true);
  }
  else {
    ledOff();
  }
  ledOff();
}

void positionCalibrateHandler(int Position){
  unsigned int i;
  for (i = 0; i < 3; i++) {
    ledBlink(500);
    if (getInputState(Position) == true) {
      break;
    }
  }
  if (i > 2) {
    for (int i = 0; i < 2; i++) {
      ledBlink(100);
    }
    storePosition(Position);
  }
  else {
    ledOff();
  }
  ledOff();
}
